<?php

namespace Utils;

class TokenFinder {
	static function getAccaunts(string $txt) : array {
		if (preg_match_all("/[A-Z]{2}\d{2}[A-Z]{4}\d{20}/", $txt, $matches)) {
			return array_unique($matches[0]);
		}
		return [];
	}

	static function getUNPs(string $txt) : array {
		if (preg_match_all("/(^|[^\d])(\d{9})($|[^\d])/", $txt, $matches)) {
			return array_unique($matches[2]);
		}
		return [];
	}

	static function getSum (string $txt) : ?float {
		if (!preg_match_all("/\d+[\.,]\d\d/", $txt, $matches)) {
			return null;
		}
		$sum = -INF;
		foreach ($matches[0] as $possible_str_sum) {
			$possible_num_sum = (float)str_replace(",", ".", $possible_str_sum);
			if ($possible_num_sum > $sum) {
				$sum = $possible_num_sum;
			}
		}
		return $sum;
	}
}