<?php

namespace Utils;

class View {
	private  $path = "";

	public static function draw(string $name, array $params) {
		$path = ROOT.'/view/'.$name.'.php';
		if (file_exists($path)) {
			extract($params);
			include $path;
		}
	}
}