<?php

namespace Utils\File2DirtyText;

class Converter {
	/*
	We must find min bound rect and rotate it.
	But it isn't php
	*/
	private static function img2txt(string $path) : string {
		$img = new \Imagick();
		$ok = $img->readImage($path);
		if (!$ok) {
			throw new IncorrectFileException;
		}
		$out = "";
		$tmp_name = ROOT.'/tmp/'.bin2hex(random_bytes(16));
		for ($i = -15; $i <= 15; $i += 5) {
			$rot = clone $img;
			$rot->rotateImage(new \ImagickPixel(), $i);
			$rot->writeImage($tmp_name);
			$rot->clear();
			$cur_out = `tesseract {$tmp_name} - -l eng`;
			if (!is_string($cur_out)) {
				throw new IncorrectFileException;
			}
			$out .= $cur_out;
		}
		$img->clear();
		unlink($tmp_name);
		return $out;
	}

	public static function convert($file) : string {
		$type = $file['type'];
		$path = $file['tmp_name'];

		switch ($type) {
			case 'image/jpeg':
			case 'image/png':
				return self::img2txt($path);
			case 'application/pdf':
				$out = `pdftotext {$path} -`;
				if (!is_string($out)) {
					throw new IncorrectFileException;
				}
				return $out;
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				$out = `docx2txt {$path} -`;
				if (!is_string($out)) {
					throw new IncorrectFileException;
				}
				return $out;
			case 'application/msword':
				$out = `catdoc {$path} -`;
				if (!is_string($out)) {
					throw new IncorrectFileException;
				}
				return $out;
			default:
				throw new UnknownTypeException;
		}
	}
}