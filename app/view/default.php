<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="static/style.css">
  <title>Новый платеж</title>
</head>
<body>
  <form enctype="multipart/form-data" action="/" method="POST">
    <input class="file" type="file" name="file">
    <input class="submit" type="submit" value="Отправить">
  </form>
  <p><?=$error;?></p>
  <label>Счет:
    <select>
      <? foreach ($accaunts as $accaunt): ?>
      <option><?=$accaunt;?></option>
      <? endforeach; ?>
    </select>
  </label>
  <label>УНП:
    <select>
      <? foreach ($UNPs as $UNP): ?>
      <option><?=$UNP;?></option>
      <? endforeach; ?>
    </select>
  </label>
  <label>Сумма: <input type="number" value="<?=$sum;?>"></label>
</body>
</html>