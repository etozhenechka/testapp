<?php

require __DIR__.'/vendor/autoload.php';

define('ROOT', __DIR__);

$params = [
	'accaunts' => [],
	'UNPs' => [],
	'sum' => 0,
	'error' => 'Загрузите файл'
];

if (isset($_FILES['file'])) {
	$params['error'] = '';
	try {
		$out = Utils\File2DirtyText\Converter::convert($_FILES['file']);
		$params['accaunts'] = Utils\TokenFinder::getAccaunts($out);
		$params['UNPs'] = Utils\TokenFinder::getUNPs($out);
		$params['sum'] = Utils\TokenFinder::getSum($out) ?? 0;
	} catch (Exception $e) {
		if ($e instanceof Utils\File2DirtyText\UnknownTypeException) {
			$params['error'] = 'Неизвестный тип файла';
		} else if ($e instanceof Utils\File2DirtyText\IncorrectFileException) {
			$params['error'] = 'Некорректный файл';
		} else {
			$params['error'] = 'Неизвестная ошибка';
		}
	}
}

Utils\View::draw('default', $params);